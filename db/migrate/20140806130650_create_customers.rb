class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.integer :coffeepunches
      t.integer :coffeesredeemed
      t.text :notes

      t.timestamps
    end
  end
end

class Customer < ActiveRecord::Base
  attr_accessible :coffeepunches, :coffeesredeemed, :name, :notes

  def can_redeem?
  	return false if coffeepunches.nil?
  	coffeepunches >= 10 
  end

  def authorized_for_redeem?(*args)
  	can_redeem?
  end  
end

class CustomersController < ApplicationController
  active_scaffold :"customer" do |conf|
  	# we don't want 'Notes' to show up on the main list
  	list.columns.exclude :created_at, :updated_at, :workrecords, :parts

  	# remove work records from show
  	show.columns.exclude :created_at, :updated_at, :workrecords, :parts
  	
  	# remove these from EVERYWHERE
  	conf.columns.exclude :created_at, :updated_at

  	# pick only the columns we want and set order
    conf.columns = [:name, :coffeepunches, :coffeesredeemed, :notes]

		conf.columns[:coffeepunches].label = "Punches"
		conf.columns[:coffeesredeemed].label = "Redeemed"

    #conf.action_links.add :index, :label => 'Show paid', :params => {:redeemed => true}, :position => false
    conf.actions = [:list, :update, :create, :search, :delete]    
    conf.action_links.add :redeem, :label => "Redeem", :type => :member, :position => false
    conf.action_links.add :purchase, :label => "New Purchase", :type => :member, :position => false

		conf.list.sorting = {:name => :asc}         
  end

  def purchase
  	@customer = Customer.find(params[:id])
  	@customer.coffeepunches = 0 if @customer.coffeepunches.nil?
  	@customer.coffeepunches += 1
  	@customer.save

    flash[:info] = "#{@customer.name} has #{@customer.coffeepunches} Punches"
    list
  end

  def redeem
  	@customer = Customer.find(params[:id])
  	if @customer.nil?
  		flash[:error] = "Can't find customer #{params[:id]}!!!!!"
  		list
  		return
  	end

 		if @customer.can_redeem?
 			@customer.coffeepunches -= 10
 			@customer.coffeesredeemed = 0 if @customer.coffeesredeemed.nil?
 			@customer.coffeesredeemed += 1
 			@customer.save
	    flash[:info] = "#{@customer.name} Redeemed Free Coffee"
	   else
	    flash[:warning] = "#{@customer.name} Does not have a free coffee yet!"
 		end
    list
  end

end
